// Update the relevant fields with the new data
var xml_data = [];
$(function () {
  var table_row = $('#table tbody tr');

  $('[name="key"]').keyup(function () {
    var key = $(this).val().toLowerCase();
  var matching_data = [];
    if (key.length == 0) {
     setTableInfo(xml_data);
    } else {
       xml_data.forEach(function(data){
         let client = data.client.toLowerCase();
         let type = data.type;
         if(typeof type != 'undefined'){
           type = type.toLowerCase();
         }else{
          type = "";
                 }
        
        if(client.includes(key) || type.includes(key)){
         // console.log("hehe" + ' | ' + data.client);
          matching_data.push(data);
        }
      });
    console.log(matching_data.length);
      setTableInfo(matching_data);
    }
  });
})
function getData(data) {
  xml_data = data;
  setTableInfo(xml_data);
}
function setTableInfo(info) {
  var table = $('#table tbody');
  table.empty();
  if (info.length != 0) {
    info.forEach(function (data) {
      var appendTable = `
      <tr>
      <td >${data.client}</td>
      <td>${data.type}</td>
      <td>${data.benchmark}</td>
      <td>${data.invoice}</td>
      <td>${data.dependents}</td>
      <td>${data.deliverables}</td>
     <td>${data.passport}</td>
       <td>${data.update_description}</td>
    </tr>   
   `;
      table.append(appendTable);
    })
  }

}

// Once the DOM is ready...
window.addEventListener('DOMContentLoaded', function () {
  // ...query for the active tab...
  chrome.tabs.query({
    active: true,
    currentWindow: true,
  }, function (tabs) {
    // ...and send a request for the DOM info...
    chrome.tabs.sendMessage(
      tabs[0].id,
      { from: 'popup', subject: 'DOMInfo' },
      // ...also specifying a callback to be called 
      //    from the receiving end (content script)
      getData);
  });
});