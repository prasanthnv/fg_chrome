function getClientExceptions(country, callback) {
    /*
        @param country : Country name (sheet name)
        @param client : client name
        @param type : case type
        @param callback : callback function to call after 
    */
    country = country.toLowerCase();
    $.getJSON(chrome.extension.getURL('./data/client_exceptions.json'), function (jsonData) {
        // for looping to get all keys
        $.each(jsonData, function (index, cases) {
            if (index.toLowerCase() === country) {
                callback(jsonData[index]);
            }
        });
    });

}