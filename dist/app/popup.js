// Update the relevant fields with the new data
function setDOMInfo(info) {

  document.getElementById('country').innerHTML = info.country;
  document.getElementById('client').innerHTML = info.client;
  document.getElementById('type').innerHTML = info.type;
  document.getElementById('benchmark').innerHTML = info.benchmark ? info.benchmark : '<span class="text text-warning">Required</span>';
  document.getElementById('invoice').innerHTML = info.invoice ? info.invoice : '<span class="text text-warning">Required</span>';
  document.getElementById('dependents').innerHTML = info.dependents ? info.dependents : '<span class="text text-warning">Required</span>';
  document.getElementById('deliverables').innerHTML = info.deliverables ? info.deliverables : '<span class="text text-warning">Required</span>';
  document.getElementById('passport').innerHTML = info.passport  ? info.passport : '<span class="text text-warning"> Required</span>';
  document.getElementById('update_description').innerHTML = info.update_description ? info.update_description : '<span class="text text-warning">Required</span>';



  if (typeof info.employment_tab == 'undefined') {
    document.getElementById('employ_tab').style.display = "none";
  }
}

// Once the DOM is ready...
window.addEventListener('DOMContentLoaded', function () {
  // ...query for the active tab...
  chrome.tabs.query({
    active: true,
    currentWindow: true,
  }, function (tabs) {
    // ...and send a request for the DOM info...
    chrome.tabs.sendMessage(
      tabs[0].id,
      { from: 'popup', subject: 'DOMInfo' },
      // ...also specifying a callback to be called 
      //    from the receiving end (content script)
      setDOMInfo);
  });
});