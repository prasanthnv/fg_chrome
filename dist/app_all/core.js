function getCaseData(country, client, type, callback) {
    /*
        @param country : Country name (sheet name)
        @param client : client name
        @param type : case type
        @param callback : callback function to call after 
    */
    country = country.toLowerCase();
    client = client.toLowerCase();
    type = type;

    let caseData = [];

    $.getJSON(chrome.extension.getURL('./data/all.json'), function (jsonData) {
       // console.log(jsonData);
        callback(jsonData);
    });

}