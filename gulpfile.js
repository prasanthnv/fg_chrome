
var gulp  = require('gulp'),
     gulpXlsx = require('gulp-js-xlsx'),
 rename = require('gulp-rename');


gulp.task('build', function () {
    gulp.src('sheets/**/*.xlsx')
        .pipe(gulpXlsx.run({
            parseWorksheet: 'tree'
        }))
        .pipe(rename({extname: '.json'}))
        .pipe(gulp.dest('app/data'));
});


gulp.task('default', [ 'build']);
