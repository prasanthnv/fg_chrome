// Update the relevant fields with the new data

function setDOMInfo(info) {
  var $table = $('.frag_table tbody');
  $.map(info, function(data, field) { 
    field = field.replace('_',' ');
    if(field == "client"){
      data = '<span class="text-capitalize">'+data.toLowerCase()+'</span>';
    }
    $table.append(`
      <tr>
        <td class="text-capitalize" nowrap>${field} :</td>
        <td align="right"><span id="benchmark">${data}</span></td>
      </tr>
    `);
   });
 
}

// Once the DOM is ready...
window.addEventListener('DOMContentLoaded', function () {
  // ...query for the active tab...
  chrome.tabs.query({
    active: true,
    currentWindow: true,
  }, function (tabs) {
    // ...and send a request for the DOM info...
    chrome.tabs.sendMessage(
      tabs[0].id,
      { from: 'popup', subject: 'DOMInfo' },
      // ...also specifying a callback to be called 
      //    from the receiving end (content script)
      setDOMInfo);
  });
});