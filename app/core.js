function getCaseData(country, client, type, callback) {
    /*
        @param country : Country name (sheet name)
        @param client : client name
        @param type : case type
        @param callback : callback function to call after 
    */
    country = country.toLowerCase();
    client = client.toLowerCase();
    type = type;

    let caseData = [];

    $.getJSON(chrome.extension.getURL('./data/client.json'), function (jsonData) {
        // for looping to get all keys

       
        $.each(jsonData, function (index, cases) {
            
            if ( index === country) {
        
                // if key equal to country
                // then get caseTypes
                // search for case types which has casetype of given type
                $.each(cases, function (index, case_obj) {
                  //  console.log(case_obj);
                    let case_type = case_obj.type.trim();
                    let case_client = case_obj.client;
                    if (typeof case_client != 'undefined') {
                        case_client = case_client.trim().toLowerCase();
                    }

                    // checking for case type
                    if (type === case_type) {
                        caseData.push(case_obj);    
                    }
                });
            }
            
        });
      let case_length = caseData.length;
      let default_country_data;
      let case_data = "";
     // console.log("case length : "+case_length);
      if(case_length > 1){
        // has more than one case types 
        // checking for client name
        caseData.forEach(function(casedata,index) {
          if(casedata.client.toLowerCase() === client.toLowerCase()){
             // remove from array
              case_data = casedata; 
          }
          if(casedata.client.toLowerCase() === country.toLowerCase()){
            default_country_data = casedata;
          }
        }, this);
      
      }else{
        case_data = caseData[0];
      }

      if(case_data == ""){
        case_data = default_country_data;
      }
       callback(case_data);
    });

}